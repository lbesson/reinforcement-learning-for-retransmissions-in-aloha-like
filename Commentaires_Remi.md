## Commentaires sur le papier WCNC

* References [3] and [14] are the same

* We could remove some references about GNU radio, we have three references on that Topic, which is quite large ([10], [18] Or [19])

* I do not really like the title, I propose (non-exhaustive list):

  * Multi-Arm Bandit in IoT Betworks: can Classical Algorithms cope with Retransmissions.
  * Reinforcement Learning for Frequency Selection in IoT Networks.
  * Upper-Confidence Bound for Frequency Selection in IoT Networks with Retransmissions.
  * Upper-Confidence Bound for Frequency Selection in ALOHA-based IoT Networks.
  * Performance of the Upper Confidence Bound Algorithm in ALOHA-based IoT Networks.

* There is a problem with affiliations, affiliation 1 should be "CentraleSupélec/IETR, CentraleSupélec Campus de Rennes, 35510 Cesson-Sévigné, France" and affiliation 2 must be "Christophe Moy, Univ Rennes, CNRS, IETR - UMR 6164, F-35000, Rennes, France".

* It would be better to start the abstract with "Internet of Things (IoT) and in particular Low Power Wide Area (LPWA) technologies".

* Sometimes acronyms are described using capital letters (Multi Arm Bandit (MAB)) sometimes without capital letters (Low power wide area (LPWA)), we need to choose.

* I think that the abstract is too general and not enough focused on the aim of the article => We do not deal much with retransmissions in the abstract. It would be better to add some concluding remarks in the abstract: "UCB1 is good and outperforms other strategies". The abstract is globally nice.

* The employed protocol is not pure ALOHA (as mentioned in the introduction), but slotted ALOHA.

* I am not sure we need to speak about implementation in the introduction.

* We need to improve the presentation of the context and to sum-up the contributions of the paper in the introduction. We can replace the penultimate paragraph by a paragraph similar to: "The aim of this paper is to assess the performance of MAB algorithms used for frequency selection in IoT networks. In particular, and compared to the literature, we focus on the impact of the retransmissions on the performance of learning algorithms. Indeed, in the case where learning algorithms are used for frequency selection, IoT devices tend to focus on a single channel, which increases the probability of having several successive collisions. The contributions of this paper can be summarized as follows:

  * We first propose a closed form approximation for the probability of having a second collision after a collision has occurred in one channel.
  * Then, we introduce several heuristics so as to cope with retransmissions.
  * We finally conduct simulations in order to compare the performance of the proposed heuristic with UCB."

* Beginning of section II, "slotted ALOHA protocols" must be replaced by "slotted ALOHA protocol".

* There are many footnotes in the paper, it would be better to include some of them in the text.

* In the system model, it would be better to remove Remove the reference to SU, PU and OSA

* We do not have any sensing in the paper. The reward is provided by the acknowledgement.

* We need to add one or two lines so as to described the

* Perhaps, it would be better to add one more section so as to separate the system model and motivations (approx for pc1).

* Beginning of section IV, "relies on" should be changed to "relies on".

